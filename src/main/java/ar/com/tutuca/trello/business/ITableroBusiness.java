package ar.com.tutuca.trello.business;

import java.util.List;

import ar.com.tutuca.trello.model.Tablero;

public interface ITableroBusiness {

    public List<Tablero> list() throws BusinessException;

    public Tablero load(int id) throws BusinessException, NotFoundException;

    public Tablero add(Tablero tablero) throws BusinessException;

    public void delete(int id) throws BusinessException;

    public Tablero update(Tablero tablero) throws BusinessException;

}