package ar.com.tutuca.trello.model.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.tutuca.trello.model.AuthToken;

@Repository
public interface AuthTokenRespository extends JpaRepository<AuthToken, String> {
}