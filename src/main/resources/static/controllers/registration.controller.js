angular.module('frontend').controller('ModalRegistrationCtrl', function ($scope, $rootScope, $uibModalInstance, userService) {
    $scope.newUser = {
        firstName: '',
        lastName: '',
        email: '',
        username: '',
        password: ''
    };

    $scope.register = function () {
        userService.agregar($scope.newUser).then(
            function () {
                $uibModalInstance.dismiss(true);
            },
            function (err) {
                $rootScope.openErrorModal(err);
            }
        );
    };
});